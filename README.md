# HELM 101

# Check minikube status
> minikube status
> minikube start

# Create helm chart
- To create new chart (opt)
- helm create buildchart

# Get the pod name
> kubectl get pods
> kubectl get pod -o wide

# Get Service
> kubectl get service
Look at the endpoint IP Address
> kubectl describe service
> kubectl get svc

# Get everything
> kubectl get all

# Helm install
- helm install name chartname/ --value chartname/values.yaml
- e:g helm install my-first-helm buildchart/ --values buildchart/values.yaml

# Port forwarding
> kubectl port-forward <PODNAME> 8080:80

# Kill everything
> helm ls 
> helm delete <names>

# Stop minikube
> minikube stop


# Helm Ref
- Helm Ref: https://opensource.com/article/20/5/helm-charts